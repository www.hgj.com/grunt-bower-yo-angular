'use strict';

/**
 * @ngdoc function
 * @name gruntBowerYoAngularApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the gruntBowerYoAngularApp
 */
angular.module('gruntBowerYoAngularApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
